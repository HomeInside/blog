
# HomeInside blog :crab:

## Cómo empezar

### Clona el repo:

```bash
$ git clone git@gitlab.com:HomeInside/blog.git
```

Este blog esta diseñado usando [Zola,](https://www.getzola.org) modificando el tema [after-dark.](https://www.getzola.org/themes/after-dark/)

**Zola** proporciona binarios preconstruidos para MacOS, Gnu/Linux y Windows en la página de [releases de GitHub](https://github.com/getzola/zola/releases).


### Pruebas en local

```bash
zola serve
```


### Revisa que todo esté bien

```bash
zola check
```


### Construir el sitio

```bash
zola build
```


## Licencia

The full text of the licenses can be found in the MIT-LICENSE' file.
