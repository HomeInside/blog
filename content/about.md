+++
title = "Acerca de"
date = "2016-02-06"
+++

Un Blog de HomeInside, un sitio en el ciberespacio para guardar notas acerca de lenguajes de programación, herramientas, lecturas y un poco más, para el futuro.

El contenido de este sitio, hará enfasis en el [idioma Español LatinoAmericano](https://es.wikipedia.org/wiki/Español_de_América), por lo que usamos muchos regionalismos para referirnos a diversos temas, y tratar de explicar conceptos.

No pretendemos la fama, ni perseguimos la idea de tener miles de segidores/likes ni nada de eso, la mayoría de la información aquí, es para tener un lugar donde almacenar conocimiento de manera pública, acerca de diversos temas, a lo largo del tiempo. Todo lo escrito aquí, son opiniones propias de los autores de este Blog, que no representan a ninguna empresa, corporación u organización y el contenido de cada entrada, siempre estará sujeto a la interpretación de los mismos.

Éste Blog, está construido con [Zola](https://www.getzola.org), un generador de sitios estáticos ([SSG](https://jamstack.org/generators/)), similar a Hugo, Pelican y Jekyll. Está escrito en [Rust](https://www.rust-lang.org) y utiliza el motor de plantillas [Tera](https://tera.netlify.app), que es similar a Jinja2, Liquid y Twig. El contenido está escrito en [CommonMark](https://commonmark.org), una especificación de [Markdown](https://www.markdownguide.org) fuertemente definida y altamente compatible.

El tema que usamos para el Blog, es una modificación de [after-dark](https://www.getzola.org/themes/after-dark/), removimos algunas clases, personalizamos los [shortcodes](https://www.getzola.org/documentation/content/shortcodes/), modificamos las citas ("blockquote") agregamos la variable `authors` a las [secciones](https://www.getzola.org/documentation/templates/pages-sections/), permitiendo que el nombre de cada autor, aparezca en la entrada que publique, y cambiamos el tema original oscuro, por el que ahora ves.

El repo de este sitio, se [encuentra en GitLab.](https://gitlab.com/HomeInside)

Cualquier información escrita aquí es de conocimiento público, obtenida de diferentes fuentes en Internet, y en la medida de lo posible, haremos referencia a ellas en las entradas de este Blog en el cual se utilicen. Sí, haremos mucho [copy/paste](https://es.wikipedia.org/wiki/Cortar,_copiar_y_pegar) de otros sitios (principalmente de sitios de habla inglesa), de donde extraemos, traducimos y organizamos la información, como siempre haciendo referencia clara al autor original (si así fuere) al final de cada entrada.

El contenido multimedia utilizado en el Blog, lo subiremos a:

- [Imgur](https://imgur.com/user/diniremix)
- [Streamable](https://streamable.com)
- [asciinema](https://asciinema.org)
- Para los fragmentos de código, usaremos el realce de sintaxis del Blog, y así facilitarle al lector el copy/paste :wink:
- [GitLab snippets](https://gitlab.com/snippets)
- ... ó en su defecto [carbon.](https://carbon.now.sh)
- [vimeo](https://vimeo.com)

En caso de que necesites contactarnos, nos encuentras en:
* [GitLab](https://gitlab.com/HomeInside)
* [Twitter](https://twitter.com/diniremix)


