+++
title = "Incrementar el número de compilación en Rust"
date = 2022-07-07

[taxonomies]
tags = ["rust"]

[extra]
authors = ["Diniremix"]
+++

:crab: Haciendo uso de Builds Scripts.
<!-- more -->

Algunos paquetes necesitan compilar código de terceros que no sea de **Rust**, por ejemplo, bibliotecas **C**. Otros necesitan funciones como la generación de código antes de la construcción ó cualquier otra cosa antes de construir.

**Cargo no pretende sustituir** a otras herramientas que están bien optimizadas para estas tareas, pero se integra con ellas con scripts de construcción personalizados. Colocar un archivo llamado **build.rs** en la raíz de un paquete hará que Cargo compile ese script y lo ejecute justo antes de construir el paquete.

{% quote() %}
Texto tomado de The Cargo Book.
{% end %}

<br>

En nuestro **Cargo.toml** agregagamos [**semver**](https://crates.io/crates/semver) como dependencia de construcción, es importante aclarar que no debe confundirse con el crate ["cargo-semver"](https://crates.io/crates/cargo-semver).

Por defecto **Build Scripts** y **build.rs** no tienen acceso a ninguna libreria/crate, para que se pueda utilizar **semver**, hay que definirla como una **dependencia de construcción** y definir el archivo inicial de construcción (build.rs).

```yaml
# Cargo.toml
...
[package]
build = "build.rs"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[build-dependencies]
semver = "1.0.12"
```

<br>

## Iniciando

En este ejemplo, vamos a generar un archivo (**build_version.conf**, en nuestro caso) para almacenar la versión incrementada, cada vez que nuestro proyecto sea compilado.

Creamos un archivo en la raíz de nuestro proyecto llamado **build.rs**

Agregamos algunos crates para manejar la creación de archivos y el versionamiento semántico de nuestra aplicación.

```rust
// build.rs
use semver::{Prerelease, Version};
use std::fs;
use std::io::Write;
use std::path::Path;
```


Definimos una constante que contiene la ruta de nuestro archivo.

```rust
// build.rs
const BUILD_FILE_NAME: &str = "target/build_version.conf";
```


Funciones para leer y crear el archivo.

```rust
// build.rs
fn read_file() -> String {
    match fs::read_to_string(BUILD_FILE_NAME) {
        Ok(c) => c,
        Err(_) => String::from("Error reading file"),
    }
}

fn save_file(data: String) {
    let mut conf_file = fs::File::create(BUILD_FILE_NAME).unwrap();
    conf_file
        .write_all(data.as_bytes())
        .expect("Error while writing to file");
}
```

<br>

## Incrementando el número de compilación en cada construcción :truck:

La función "**generate_version**", toma la versión actual (inicialmente del archivo **Cargo.toml**) e incrementa el número de compilación, generando un **Prerelease** (valiendose del crate semver) utilizado para generar el build.

Veamos un poco más de cerca esto. En nuestro archivo **Cargo.toml** tenemos algo como esto:

La versión indica "1.0.0"

```yaml
# Cargo.toml
...
[package]
name = "awesome-app"
version = "1.0.0"
edition = "2018"
build = "build.rs"
```

Utilizando el crate semver parseamos el numero de versión("1.0.0"), generamos un [prerelase](https://docs.rs/semver/1.0.4/semver/struct.Prerelease.html), un identificador opcional de pre-lanzamiento en la cadena de versión, Ejemplos:

- 0.7.0-alpha.1
- 1.0.0-beta.8
- 1.0.0-rc.0
- 0.11.0-shitshow
- 0.0.0-reserved

etc, etc,.. el nombre lo defines tú, para nuestro caso usaremos el prefijo "build."

Para finalizar esta parte, una vez se genere una construcción, la versión quedará de la siguiente manera:

versión inicial: **1.0.0**

despues de una construcción: **1.0.0-build.1**, incrementando el "build", en cada construcción. :smile:

```rust
// build.rs
fn generate_version(pkg_version: &str) -> String {
    let mut current_version = Version::parse(pkg_version.trim()).unwrap();
    let mut new_build: i32 = 1;

    if !current_version.pre.is_empty() {
        let current_build = current_version.pre.as_str();
        let (_, build_number) = current_build.split_at(6); // extract "build."
        println!("cargo:warning=current build_number: {}", build_number);
        new_build = build_number.parse::<i32>().unwrap_or(0);
        new_build += 1;
    }

    let build_text = format!("build.{}", new_build);
    current_version.pre = Prerelease::new(build_text.as_str()).unwrap();
    println!("cargo:warning=generating build version: {}", build_text);

    current_version.to_string()
}
```

<br>

## Generando el archivo o actualizándolo según sea el caso

La funcion "**update_build_version**", revisa si nuestro archivo de construcción existe, en ese caso leerá el archivo y actualizará la versión, en caso contrario generará un nuevo archivo, con la información de la variable [**CARGO_PKG_VERSION**](https://doc.rust-lang.org/cargo/reference/environment-variables.html) que ésta a su vez, extrae del archivo **Cargo.toml**

```rust
// build.rs
fn update_build_version() {
    let content_file: String;

    if Path::new(BUILD_FILE_NAME).exists() {
        content_file = read_file();
    } else {
        let pkg_version = env!("CARGO_PKG_VERSION");
        content_file = String::from(pkg_version);
    }

    let result = generate_version(content_file.as_str());
    save_file(result);
}
```

<br>

## Finalizando

En nuestra función **main**, empezamos el proceso de generar el build en cada compilación, llamando a la función **update_build_version**.

La instrucción **rerun-if-changed=build.rs** en el **println!** le indica a **Cargo** cuándo debe volver a ejecutar el script, en este caso si el archivo **build.rs** es modificado. [Más info](https://doc.rust-lang.org/cargo/reference/build-scripts.html#outputs-of-the-build-script)

```rust
// build.rs
fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    update_build_version();
}
```

<br>

## Toque final

Nuestro archivo **build.rs** debe lucir así:

```rust
// build.rs
use semver::{Prerelease, Version};
use std::fs;
use std::io::Write;
use std::path::Path;

const BUILD_FILE_NAME: &str = "target/build_version.conf";

fn read_file() -> String {
    match fs::read_to_string(BUILD_FILE_NAME) {
        Ok(c) => c,
        Err(_) => String::from("Error reading file"),
    }
}

fn save_file(data: String) {
    let mut conf_file = fs::File::create(BUILD_FILE_NAME).unwrap();
    conf_file
        .write_all(data.as_bytes())
        .expect("Error while writing to file");
}

fn generate_version(pkg_version: &str) -> String {
    let mut current_version = Version::parse(pkg_version.trim()).unwrap();
    let mut new_build: i32 = 1;

    if !current_version.pre.is_empty() {
        let current_build = current_version.pre.as_str();
        let (_, build_number) = current_build.split_at(6); // extract "build."
        println!("cargo:warning=current build_number: {}", build_number);
        new_build = build_number.parse::<i32>().unwrap_or(0);
        new_build += 1;
    }

    let build_text = format!("build.{}", new_build);
    current_version.pre = Prerelease::new(build_text.as_str()).unwrap();
    println!("cargo:warning=generating build version: {}", build_text);

    current_version.to_string()
}

fn update_build_version() {
    let content_file: String;

    if Path::new(BUILD_FILE_NAME).exists() {
        content_file = read_file();
    } else {
        let pkg_version = env!("CARGO_PKG_VERSION");
        content_file = String::from(pkg_version);
    }

    let result = generate_version(content_file.as_str());
    save_file(result);
}

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    update_build_version();
}

```


<br>

## A tener en cuenta

- No olvides definir la llave "build-dependencies" en el archivo **Cargo.toml** y en la llave "package" el campo **build**.
  
- El archivo de construcción se encuentra localizado en: "**target/build_version.conf**", **aquí no se modifica** el archivo **Cargo.toml**, por lo que si haces una limpieza (ej: `cargo clean`), el número de compilación se perderá.

- **No, no recomendamos** usar el archivo **build.rs** para modificar el archivo **Cargo.toml**, leyendo el archivo y extrayendo la información de la versión, [ya que causa problemas](https://es.wikipedia.org/wiki/Curva_cerrada_de_tipo_tiempo) al lazar una construcción, modificar el archivo **Cargo.toml**, actualizarlo y compilar tu aplicación.

- [toml](https://crates.io/crates/toml) es un excelente crate para manejar archivos [TOML](https://toml.io/en/).

- Sí lo que quieres, es modificar el archivo **Cargo.toml** y actualizar la versión, utilizando versionamiento semántico, lo mejor sería utilizar el crate [cargo-semver](https://crates.io/crates/cargo-semver)

- Sí se invoca directamente a **println!** en el archivo **build.rs**, esta no imprimirá nada por la consola, a menos que se especifique "cargo:warning", ejemplo:

```rust
println!("cargo:warning=Mensaje aqui...");
```

- Revisa el apartado de "Environment Variables" de The Cargo Book, para más información de las variables de entorno.

<br>

## Referencias

- [Versionado Semántico](https://semver.org/lang/es/)
- [semver struct.Prerelease](https://docs.rs/semver/1.0.4/semver/struct.Prerelease.html)
- [Cargo Environment Variables](https://doc.rust-lang.org/cargo/reference/environment-variables.html)
- [Build dependencies](https://doc.rust-lang.org/cargo/reference/specifying-dependencies.html#build-dependencies)
- [semver crate](https://crates.io/crates/semver)
- [Build Scripts](https://doc.rust-lang.org/cargo/reference/build-scripts.html)
- [Outputs of the Build Script](https://doc.rust-lang.org/cargo/reference/build-scripts.html#outputs-of-the-build-script)
- [cargo-semver crate](https://crates.io/crates/cargo-semver)
- [toml crate](https://crates.io/crates/toml)
