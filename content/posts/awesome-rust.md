+++
title = "awesome Rust"
date = 2021-09-14

[taxonomies]
tags = ["rust"]

[extra]
authors = ["Diniremix"]
+++

:crab: Una lista muy personal de recursos de [Rust](https://www.rust-lang.org)
<!-- more -->


Última actualización: 2022-02-15

- [Esta semana en Rust](https://this-week-in-rust.org) Actualizaciones de Rust seleccionadas a mano.

- [rustup](https://rustup.rs/) para instalar **Rust.** :wink:

- [cargo](https://doc.rust-lang.org/cargo/) Cargo es el gestor de paquetes de Rust. Cargo descarga las dependencias de tu proyecto Rust, compila, hace paquetes distribuibles y los sube a [crates.io](https://crates.io), el registro de paquetes de la comunidad Rust.

- [rustfmt](https://github.com/rust-lang/rustfmt) Una herramienta para formatear el código Rust de acuerdo a las normas de estilo.

- [cargo-make](https://crates.io/crates/cargo-make) Herramienta de ejecución de tareas personalizadas, usando **Cargo.**

- [clippy](https://github.com/rust-lang/rust-clippy) Una colección de lints para detectar errores comunes y mejorar tu código Rust.

- [rusqlite](https://crates.io/crates/rusqlite) Es un wrapper ergonómico para usar [SQLite](https://www.sqlite.org/index.html).

- [term-table](https://crates.io/crates/term-table) Renderiza tablas en la terminal (CLI).

- [rand](https://crates.io/crates/rand) Una librería para la generación de números aleatorios.

- [chrono](https://crates.io/crates/chrono) Librería para el manejo de fecha y hora.

- [firestore-db-and-auth](https://crates.io/crates/firestore-db-and-auth) Permite de forma sencilla el acceso desde Rust, a tu base de datos de [Google Firestore](https://firebase.google.com/products/firestore) a través de una **cuenta de servicio** ó de **credenciales OAuth.**

- [wasm-pack](https://crates.io/crates/wasm-pack) Te ayuda a construir paquetes de [WebAssembly](https://webassembly.org]) de una manera muy rápida y sencilla.



Esta lista, se irá actualizando en la medida de ir conociendo y utilizando nuevas librerías.

Para más información, consulta la [documentación oficial de Rust](https://doc.rust-lang.org/stable/book/)

