+++
title = "notas sobre IndexedDB"
date = 2020-11-21

[taxonomies]
tags = ["JS"]

[extra]
authors = ["Diniremix"]
+++

IndexedDB es una API de javaScript de bajo nivel que ofrece almacenamiento del lado del cliente.
<!-- more -->


Permitiendo además almacenar de cantidades significativas de datos estructurados, incluyendo archivos y blobs.

Para búsquedas de alto rendimiento en esos datos, se usa índices, permitiendo busquedas más veloces sobre los datos guardados.
Mientras [DOM Storage](https://developer.mozilla.org/es/docs/Web/API/Web_Storage_API) es útil para el almacenamiento de pequeñas cantidades de datos, no es útil para almacenar grandes cantidades de datos estructurados. IndexedDB proporciona una solución.


<br>

## Qué tiene que ver esto de IndexedDB con localStorage/sessionStorage?

En la superficie, las dos tecnologías pueden parecer directamente comparables, sin embargo, si pasa algo de tiempo con ellas, pronto se dará cuenta de que no lo son. Fueron diseñados para lograr un objetivo similar, **el almacenamiento del lado del cliente**, pero abordan la tarea en cuestión desde perspectivas significativamente diferentes y funcionan mejor con diferentes cantidades de datos.

localStorage, o más precisamente Web Storage, fue diseñado para cantidades más pequeñas de datos. Básicamente, se trata de una cadena solo para almacenamiento de valor, con una API síncrona simplista.

**indexedDB**, por otra parte, fue diseñado para trabajar con cantidades de datos significativamente mayores. Primero, en teoría, proporciona tanto una API síncrona como una asíncrona. Sin embargo, en la práctica, todas las implementaciones actuales son asíncronas y las solicitudes no bloquearán la carga de la interfaz de usuario.


En una reciente revisión de la documentación nos dice:

{% quote(author="MDN Web Docs") %}
Las operaciones realizadas con IndexedDB se hacen de forma asíncrona, para no bloquear las aplicaciones. IndexedDB incluía originalmente APIs síncronas y asíncronas. La API sincrónica estaba pensada para ser utilizada únicamente con Web Workers, pero se eliminó de la especificación porque no estaba claro si era necesaria. Sin embargo, la API sincrónica podría reintroducirse si hay suficiente demanda por parte de los desarrolladores web.
{% end %}

<br>

## Qué navegadores lo soportan?
[can I use IndexedDB?](https://caniuse.com/?search=IndexedDB)


<br>

## Para qué me sirve esto?
- sesiones
- perfiles de usuario
- almacen de datos (funciona como una base de datos local)
- PWA's como la de [Pokedex](https://pokedex.org)


<br>

## Hay Límites de almacenamiento?
No existe un límite de tamaño para un elemento simple de la base de datos. Sin embargo, puede haber un [límite en el tamaño de cada base de datos IndexedDB.](https://developer.chrome.com/apps/offline_storage#temporary) Este límite (y la forma en que la interfaz de usuario la hace valer) [puede variar de un navegador a otro.](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API/Browser_storage_limits_and_eviction_criteria)

Aunque por lo general cada navegador maneja diferentes tipos de almacenamiento de datos, el almacenamiento viene en dos tipos:

- **Temporal:** Son datos que no necesitan persistir durante mucho tiempo. Serán eliminados bajo una política de uso menos reciente, cuando se alcancen los límites de almacenamiento.
- **Persistente:** Estos son datos que están destinados a mantenerse durante mucho tiempo. Sólo se desalojarán si el usuario lo decide.

Por ejemplo, en Firefox el espacio máximo de almacenamiento del navegador es dinámico, se basa en el tamaño de su disco duro. El límite global se calcula como el 50% del espacio libre en disco. Cuando se utiliza el almacenamiento persistente, el usuario recibe una ventana emergente de la interfaz de usuario para avisarle de que estos datos van a persistir, **y le pregunta si está conforme con ello**. El almacenamiento temporal de datos no provoca ninguna pregunta al usuario.


Mientras que en Chome, el almacenamiento temporal se comparte entre todas las aplicaciones web que se ejecutan en el navegador. El pool compartido puede ser de hasta 1/3 del espacio de disco disponible. Y una aplicación puede tener una cuota mayor para el almacenamiento persistente que para el temporal, pero debe solicitarlo mediante la API de gestión de cuotas y **el usuario debe concederle permiso para utilizar más espacio.**

<br>

## Empezando:

- [IndexedDB Api en developer mozilla](https://developer.mozilla.org/es/docs/Web/API/IndexedDB_API)
- [Conceptos básicos](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API)
- [Usando IndexedDB](https://developer.mozilla.org/es/docs/Web/API/IndexedDB_API/Usando_IndexedDB)


<br>

## Librerías?
- [Dexie.js (recomendada) :tada: :rocket:](https://dexie.org)
- [localForage](https://localforage.github.io/localForage/)
- [LokiJS](http://techfort.github.io/LokiJS/)
- [ImmortalDB](https://github.com/gruns/ImmortalDB)
- y unas [cuantas más](https://opensourcelibs.com/libs/indexeddb) :smile:


<br>

## Referencias

- [IndexedDB Api en developer mozilla](https://developer.mozilla.org/es/docs/Web/API/IndexedDB_API)
- [Una guía rápida](https://www.freecodecamp.org/news/a-quick-but-complete-guide-to-indexeddb-25f030425501/)
- [Almacenar imágenes y archivos en IndexedDB](https://hacks.mozilla.org/2012/02/storing-images-and-files-in-indexeddb/)
- [PWA para los fans de Pokémon](https://pocketjavascript.com/blog/2015/11/23/introducing-pokedex-org)
- [Almacenamiento (y recuperación) de fotos en IndexedDB](https://www.raymondcamden.com/2018/10/05/storing-retrieving-photos-in-indexeddb)
- [un gist del artículo anterior :point_up:](https://gist.github.com/robnyman/1894032)
- [Un primer paseo por IndexedDB](https://hacks.mozilla.org/2010/06/comparing-indexeddb-and-webdatabase/)
- [Mejores prácticas para el uso de IndexedDB](https://developers.google.com/web/fundamentals/instant-and-offline/web-storage/indexeddb-best-practices?authuser=1)
