+++
title = "Hola Rust"
date = 2021-09-16

[taxonomies]
tags = ["rust"]

[extra]
authors = ["Diniremix"]
+++

:crab: Un lenguaje que empodera a todos para construir software fiable y eficiente.
<!-- more -->


```rust
fn main() {
    println!("Hola mundo!");
}
```

## Comienza con Rust

{% quote(author="rust website") %}
Rust es increíblemente rápido y eficiente en cuanto a memoria: sin tiempo de ejecución ni recolector de basura, puede alimentar servicios de rendimiento crítico, funcionar en dispositivos integrados e integrarse fácilmente con otros lenguajes.
{% end %}

["El libro"](https://doc.rust-lang.org/book/), **The Rust Programming Language,** te dará una visión del lenguaje desde los principios básicos. Construirás unos cuantos proyectos por el camino y, al final, tendrás una comprensión sólida del lenguaje.

La comunidad de Rust tiene un canal de Youtube con un gran número de charlas y tutoriales.

[Rust en Youtube](https://www.youtube.com/channel/UCaYhcUwRBNscFNUKTjgPFiA)
