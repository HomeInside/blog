+++
title = "Qt y WebAssembly parte 2"
date = 2022-02-03

[taxonomies]
tags = ["Qt", "wasm"]

[extra]
authors = ["Diniremix"]
+++

QT y WebAssembly, a la Web y más allá.
<!-- more -->

[La parte 1, disponible aquí](../wasm-qt-part1).

En esta segunda (y ultima?) parte, crearemos un proyecto en Qt, compilaremos y ejecutaremos en la web (servidor local) nuestra aplicación para WebAssembly.


## Configurando nuestro Proyecto para WebAssembly

Abrimos nuestro proyecto en **Qt Creator** y en la parte izquierda del IDE, seleccionamos la opción **Qt 5.15.2 WebAssembly** y luego en botón "**Configure Project**".

{{ imgur(id="https://i.imgur.com/LjKUMUf.png") }}


En la parte inferior aparecerá el kit de **Qt 5.15.2 WebAssembly** y el logo de WebAssembly, además de los perfiles de construcción.
{{ imgur(id="https://i.imgur.com/IAcYyGQ.png") }}

<br>


## Compilando nuestra aplicación

Una vista previa de la aplicación:

{{ imgur(id="https://i.imgur.com/kAmAWtx.png") }}

En la parte inferior aparecerá el kit de **Qt 5.15.2 WebAssembly** y el logo de WebAssembly, además de los perfiles de construcción.

Construimos ó lanzamos nuestra aplicación, y listo, :tada: :rocket: 

Recuerda que la primera vez que se ejecute, tomará un tiempo mientras se genera el archivo wasm

{{ imgur(id="https://i.imgur.com/rDFFiDI.png") }}

<br>


## Qt y WebAssembly

En la carpeta de construcción de nuestro proyecto, además de los archivos de compilación habituales de un proyecto de Qt, veremos los siguientes archivos:

- qtloader.js
- qtlogo.svg
- myWasmQtProject.html (un archivo html para ejecutar nuestra aplicación en un servidor web.)
- myWasmQtProject.js
- myWasmQtProject.js_plugin_import.cpp
- myWasmQtProject.js_plugin_import.o
- myWasmQtProject.wasm (nuestra aplicación en formato WebAssembly :smile:)

<br>


## Qt en la Web!

nuestro proyecto ejecutandose en el navegador! :smile: :tada: :rocket:
{{ imgur(id="https://i.imgur.com/FvLot3n.png") }}

Hello World Qt
{{ imgur(id="https://i.imgur.com/MSYdmRl.png") }}

<br>


## Consideraciones

Qt y WebAssembly son herramientas poderosas que nos permiten llevar las aplicaciones tradicionales y darle una vuelta de tuerca para desplegarlas en la web, sin apenas esfuerzo adicional por parte del desarrollador.

Sin embargo al ser una tecnologia "reciente", WebAssembly, aún tiene [algunas limitaciones](https://doc.qt.io/qt-5/qtwebassembly-platform-notes.html#known-limitations), como el almacenamiento, la concurrencia, acceso a las fuentes del sistema, y temas relacionados con QNetworkAccessManager.

<br>

## Referencias

- [Building Qt for WebAssembly (gist)](https://gist.github.com/cajus/689eca85be257997019065979799e2f6)
- [Qt, Building Applications for the Web](https://doc.qt.io/qtcreator/creator-setup-webassembly.html)
- [emscripten download and install](https://emscripten.org/docs/getting_started/downloads.html)
- [WebAssembly Concepts](https://developer.mozilla.org/en-US/docs/WebAssembly/Concepts)
- [Compiling a New C/C++ Module to WebAssembly](https://developer.mozilla.org/en-US/docs/WebAssembly/C_to_wasm)
- [Qt & WebAssembly Known limitations](https://doc.qt.io/qt-5/qtwebassembly-platform-notes.html#known-limitations)
- [What are the limitations?](https://subscription.packtpub.com/book/web_development/9781788997379/1/ch01lvl1sec13/what-are-the-limitations)
