+++
title = "Limpiar la pantalla en Rust"
date = 2021-10-11

[taxonomies]
tags = ["rust"]

[extra]
authors = ["Diniremix"]
+++

:crab: muchas maneras, el mismo efecto?
<!-- more -->

Mientras desarrollamos una aplicación que utiliza la línea de comandos, una de las tareas más triviales y necesaria (en muchos casos) es limpiar la pantalla....

Rust provee un par de opciones para tal fin.


Utilizando **std**

```rust
use std::process;

fn clear_screen() {
    process::Command::new("clear").status().unwrap();    
}
```

funciona muy bien en sistemas Gnu/Linux y Mac OS.
{% quote() %}
En sistemas Windows, basta con cambiar "clear" por "cls"
{% end %}


<br>

Usando códigos de escape ANSI. Todos los códigos de escape comienzan por los caracteres **ESC** (carácter ASCII número 27 decimal, **1B** en hexadecimal) seguido del carácter **[**, tienen cero o más parámetros delimitados por **;**

```rust
fn clear_screen() {
    print!("\x1B[2J");
}
```


<br>

Una variante del anterior, que posiciona el cursor en la fila 0, columna 0.

```rust
fn clear_screen() {
    print!("\x1B[2J \x1B[0;0f");
}
```


<br>

Otro ejemplo casteando el número 27 como **char**.


```rust
fn clear_screen() {
    print!("{}[2J", 27 as char);
}
```


<br>

## Referencias

- [Códigos de escape_ANSI](https://es.wikipedia.org/wiki/Código_escape_ANSI)
- [termion](https://github.com/redox-os/termion/blob/master/src/clear.rs)
- [clearscreen](https://github.com/watchexec/clearscreen)
- [stackoverflow](https://stackoverflow.com/questions/4062045/clearing-terminal-in-linux-with-c-code/4062051#4062051)
- [rosettacode](http://rosettacode.org/wiki/Terminal_control/Clear_the_screen#Rust)
