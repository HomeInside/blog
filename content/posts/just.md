+++
title = "Just, Automatizando tus tareas habituales"
date = 2021-11-10

[taxonomies]
tags = ["just"]

[extra]
authors = ["Diniremix"]
+++

:robot: automatizar!...
<!-- more -->

En algún punto de nuestros proyectos generamos scripts, para automatizar algunos procesos, o simplemente tenemos a la mano decenas de comandos (ó alias para ellos) que hacen despliegues, construyen sitios, generan claves aleatorias, consultan el [estado del clima](https://wttr.in), la [IP](https://icanhazip.com) etc...

Y al final del día, tenemos que recordar mil comandos para tareas repetitivas, y es ahí donde brilla [**Just**](https://just.systems)
un programa para ejecutar tareas, otros programas, un automatizador para dominarlos a todos...


**Just** provee una sintaxis inspirada en **make**, para crear las recetas, además de:

- funciones predefinidas
- uso de variables de entorno
- manipulación de strings
- manejo de rutas
- evaluación de comandos
- expresiones condicionales
- extender las recetas con scripting en Bash, Python, Ruby, NodeJS, Perl

y un [largo etcétera](https://cheatography.com/linux-china/cheat-sheets/justfile/) de funcionalidades increibles, para la creación de recetas a tu gusto.

Ejemplo:

```make
nombre-de-tu-receta:
  echo 'Esto es una receta usando Just!'

# esto es un comentario
otra-receta:
  @echo 'Esto es otra receta.'
```

{% quote(author="un visitante del blog") %}
En Just, los comandos, llamados recetas, se almacenan en un archivo llamado .justfile, con una sintaxis inspirada en make
{% end %}

**Just** se puede utilizar de varias maneras dependendiendo tus necesidades, sin embargo las maneras habituales son:

- De forma **local,** creando un archivo llamado **.justfile** en la raiz de tu projecto.
- De forma **global**, creando varias tareas de proposito general.

<br>


# Just de forma local

El archivo **.justfile** se almacena en la raiz de tu projecto.

Veamos un ejemplo:

Creamos una **receta** llamada **hello** que acepta un parámetro llamado **guy.** Esta receta, invoca a **echo** y le envía el parámetro establecido.

```make
# .justfile
hello guy:
    @echo "Hello {{ guy }}!"
```

y al lanzar nuestra receta, tendremos:

```sh
$ just hello Tux
Hello Tux!
```
Un archivo **.justfile** con recetas en una carpeta de un proyecto de software, puede entre otras cosas, construir el proyecto, lanzar pruebas unitarias, construir la documentación, hacer deploy a un servidor web, etc. [Las posibilidades son infinitas.](https://github.com/casey/just/tree/master/examples)

<br>


# Just de forma global
para usar **Just** de forma global es [necesario organizar un archivo](https://github.com/casey/just#user-justfiles), usualmente llamado **.user.justfile,** con nuestras recetas y alojarlo en la carpeta **$HOME** de nuestro Sistema Operativo.

Luego, crear un alias (recomendado) para que **Just**, al ser lanzado cargue ese archivo de recetas.

Ejemplo, en nuestro archivo **.bashrc** ó **.zshrc** agregar un nuevo alias:

```bash
alias .j='just -f ~/.tu_nombre_de_usuario.justfile --working-directory .'
```

Nuestro alias sera "**.j**" y podemos utilizarlo como:

```sh
$ .j nombre-receta
```

Esto aplica para cualquier receta ubicada en **~/.user.justfile**.

Ejemplo:

Crearemos una **receta** llamada **repo** que acepta un parámetro llamado **folder.** Esta receta, crea una carpeta e inicializa **git flow**.

```make
# .your_user.justfile
current_path := invocation_directory()

# configurar un nuevo repo usando git flow
repo folder:
    @echo "creando repo en {{ current_path }}/{{ folder }}"
    @echo "directorio de trabajo: {{ folder }}"
    @mkdir -pv {{ folder }}
    @echo
    @echo "iniciando git flow..."
    @cd {{ folder }} && git flow init -fd
    @echo
    @echo "proceso completado en: {{ current_path }}/{{ folder }}"
```

y al lanzar nuestra receta, tendremos:

```sh
$ .j repo awesome-frontend
creando repo en /home/diniremix/dev/awesome-frontend
directorio de trabajo: awesome-frontend
mkdir: se ha creado el directorio 'awesome-frontend'

iniciando git flow...
Inicializado repositorio Git vacío en /home/diniremix/dev/awesome-frontend/.git/
Using default branch names.
No branches exist yet. Base branches must be created now.
Branch name for production releases: [master] 
Branch name for "next release" development: [develop] 

How to name your supporting branch prefixes?
Feature branches? [feature/] 
Bugfix branches? [bugfix/] 
Release branches? [release/] 
Hotfix branches? [hotfix/] 
Support branches? [support/] 
Version tag prefix? [] 
Hooks and filters directory? [/home/diniremix/dev/awesome-frontend/.git/hooks] 

proceso completado en: /home/diniremix/dev/awesome-frontend
```

<br>


## Extendiendo Just con Scripts

En algunas ocasiones queremos ir un poco más allá con nuestras recetas, **Just** provee un mecanismo sencillo para extenderlas, usando Bash, Python, y otros lenguajes de script.

Lo único que tienes que hacer, es iniciar la receta con "**#!**"

Ejemplo:

```make
# .justfile

receta-python:
  #!/usr/bin/env python3
  print('Hola desde python!')

receta-js:
  #!/usr/bin/env node
  console.log('Saludos desde JavaScript!')

receta-perl:
  #!/usr/bin/env perl
  print "Larry Wall dice Hola!\n";

receta-sh:
  #!/usr/bin/env sh
  set -euxo pipefail
  hello='Yo'
  echo "$hello desde un shell script!"

receta-ruby:
  #!/usr/bin/env ruby
  puts "Hola desde ruby!"
```

<br>

Algunas de las recetas habituales son:

- crear una aplicacion/libreria en **Rust**
- crear un proyecto con **Python**, virtualenv y pip
- generar claves de SSH
- desplegar un backend de servicios en GAE/Heroku
- ver el clima :smile:
- la IP actual :wink:
- mostrar estadisticas de un proyecto, y la cantidad de lineas de código, con [tokei.](https://github.com/XAMPPRocky/tokei)

<br>


## Referencias

- [Just systems](https://just.systems)
- [casey/just](https://github.com/casey/just)
- [Just cheat sheets](https://cheatography.com/linux-china/cheat-sheets/justfile/)
- [User justfiles](https://github.com/casey/just#user-justfiles)
- [Just use just](https://toniogela.dev/just/)
- [wttr.in](https://wttr.in)
- [tokei](https://github.com/XAMPPRocky/tokei)
