+++
title = "Qt y WebAssembly parte 1"
date = 2022-02-02

[taxonomies]
tags = ["Qt", "wasm"]

[extra]
authors = ["Diniremix"]
+++

QT y WebAssembly, a la Web y más allá.
<!-- more -->


[WebAssembly](https://webassembly.org]) abreviado Wasm, es un formato de codigo binario portable (bytecode).
Wasm está diseñado como un objetivo de compilación portátil para lenguajes de programación, lo que permite el despliegue en la web de aplicaciones de cliente y servidor.

Y... en este caso usaremos [Qt](https://www.qt.io) :wink:

<br>

{% quote() %}
En esta primera parte, instalaremos Emscripten y configuraremos Qt para para compilar aplicaciones a WebAssembly.
{% end %}

<br>


## Requisitos

- [Qt 5.12 ó superior](https://www.qt.io) :smile:
- [Emscripten](https://emscripten.org/docs/introducing_emscripten/index.html)

<br>


## Empezando

En este apartado asumimos que tienes instalado **Qt 5.12** ó superior, una forma sencilla es obtener el [instalador online](https://www.qt.io/download-qt-installer) y seguir los pasos de instalación.

Clonamos el repo de **Emscripten**:
```sh
$ git clone https://github.com/emscripten-core/emsdk.git
```

```sh
$ cd emsdk
```

Para **Qt 5.15.2** (la que usaremos en los ejemplos) la [versión recomendada](https://doc.qt.io/qt-5/wasm.html) de **Emscripten** es: **1.39.8**

Cada versión menor de **Qt** soporta una versión de **Emscripten** conocida como "buena". Esa versión de Emscripten será soportada durante la vida de esa versión de Qt.

ejemplos:

- **Qt 5.12:** 1.38.16
- **Qt 5.13:** 1.38.27 (multithreading: 1.38.30)
- **Qt 5.14:** 1.38.27 (multithreading: 1.38.30)
- **Qt 5.15:** 1.39.8 (la que utilizaremos en nuestro caso)

instalamos el SDK:

```sh
$ ./emsdk install sdk-1.39.8-64bit
```

{{ imgur(id="https://i.imgur.com/whqgbYB.png") }}

una vez finalice la instalación, podemos comprobar que todo esté en orden:

```sh
$ cd /path/to/emsdk
```

Revisamos que la versión instalada sea la correcta, para nuestra versión de **Qt**

```sh
$ ./emsdk list
All recent (non-legacy) installable versions are:
         1.39.8    INSTALLED
```

{% quote(author="un visitante del blog") %}
Es posible que Qt "no encuentre" el sdk de emscripten, en ese caso, agrega la carpeta de instalacion al path.
{% end %}

Si utilizas **bash**

```sh
$ echo 'source "/path/to/emsdk/emsdk_env.sh"' >> $HOME/.bash_profile
```

Si utilizas **zsh**

```sh
$ echo 'source "/path/to/emsdk/emsdk_env.sh"' >> $HOME/.zprofile
```


Activamos la versión de emscripten que acabamos de instalar

```sh
$ emsdk activate 1.39.8
```

Revisamos la versión de **em++** (binario de **emscripten** para **C++**)

```sh
$ em++ --version
emcc (Emscripten gcc/clang-like replacement) 1.39.8 (commit bc6209b6dc30a9ecf99eda8eecc397a14ccd785a)
Copyright (C) 2014 the Emscripten authors (see AUTHORS.txt)
This is free and open source software under the MIT license.
There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

Revisamos la versión de **emcc** (binario de **emscripten** para **C**)
```sh  
$ emcc --version
emcc (Emscripten gcc/clang-like replacement) 1.39.8 (commit bc6209b6dc30a9ecf99eda8eecc397a14ccd785a)
Copyright (C) 2014 the Emscripten authors (see AUTHORS.txt)
This is free and open source software under the MIT license.
There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

Revisamos que **emrun,** esté en el path.

```sh
$ emrun --help
usage: emrun [emrun_options] filename.html [html_cmdline_options]

   where emrun_options specifies command line options for emrun itself, whereas
   html_cmdline_options specifies startup arguments to the program.

```

<br>


## Configurando Qt para WebAssembly
**1.**

Abrimos el menu **Help** --> **About Plugins** y revisamos si el plugin para WebAssembly está activado.

Buscamos por: **WebAssembly(experimental)**

usando a shorcut
{{ imgur(id="https://i.imgur.com/85TBB43.png") }}


**2.**

Abrimos el menu **Tools** --> **Options**

Opción **Devices** y luego el tab **WebAssembly**

y agregamos la ruta de la carpeta donde instalamos el **SDK** de **Emscripten**(emsdk)
{{ imgur(id="https://i.imgur.com/85TBB43.png") }}


**3.**

Nos trasladamos al tab **Devices** y revisamos que exista la opción "**Web Browser (default for Web Browser)**"
{{ imgur(id="https://i.imgur.com/qYbxNid.png") }}


**4.**

En la parte superior izquierda se encuentra la opción **Kits** y nos movemos al tab **Compilers** 

Las opciones "**Emscripten Compiler 1.39.8 for C**" y "**Emscripten Compiler 1.39.8 for C++**" respectivamente, deben aparecer

{{ imgur(id="https://i.imgur.com/FHb6Oye.png") }}

En caso de que no, debemos agregar manualmente, con el botón **Add**, ubicado en la parte superior derecha, seleccionar la opción **Emscripten** y luego en **C** y después **C++**.

{{ imgur(id="https://i.imgur.com/ogFGFFQ.png") }}

{{ imgur(id="https://i.imgur.com/mnBGZz8.png") }}

la opción **ABI** debe quedar fijada en:

```
<custom> asmjs unknown unknown emscripten 32bits
```
tanto para **C,** como para **C++.**


**5.**
nos pasamos al tab "**Qt Versions**" y chequeamos que exista una entrada de **qmake** para WebAssembly.

{{ imgur(id="https://i.imgur.com/3MiFxKD.png") }}

**6.**
Finalmente en el tab "**Kits**" revisamos que todo está en orden:
{{ imgur(id="https://i.imgur.com/tm5rxNq.png") }}

{% quote(author="un visitante del blog") %}
Un poco complejo no?...
{% end %}

Sí, parece un poco tedioso, pero lo bueno, es que esta configuración, solo la realizas una vez. :smile:

En la [próxima entrada](../wasm-qt-part2), crearemos un proyecto en Qt y usando la configuración citada arriba, compilaremos y ejecutaremos en la web (servidor local) nuestra aplicación para WebAssembly.


<br>


## Referencias

- [WebAssembly](https://webassembly.org)
- [Qt for WebAssembly](https://doc.qt.io/qt-5/wasm.html)
- [Introducing Emscripten](https://emscripten.org/docs/introducing_emscripten/index.html)
- [Qt WebAssembly Demos & Examples](https://www.qt.io/qt-examples-for-webassembly)
- [Qt WebAssembly Platform Notes](https://doc.qt.io/qt-5/qtwebassembly-platform-notes.html)
