+++
title = "DockerFile para applicaciones en Rust"
date = 2018-05-01

[taxonomies]
tags = ["rust", "rocket"]

[extra]
authors = ["Diniremix"]
+++

Creando una imagen de [Docker](https://www.docker.com) para una aplicación en Rust.
<!-- more -->

Este archivo, se extrajo del repositorio [rocket-rust](https://github.com/HomeInside/rocket-rust) facilitando la creación de una imagen de Docker para aplicaciones en [Rust](rust-lang.org), usando [Rocket](https://rocket.rs/v0.4/guide/) como Web framework.

```docker
FROM ubuntu:16.04

MAINTAINER Awesome User <awesome-user@example.como>

RUN apt-get update \
    && apt-get install -y curl nano file net-tools build-essential

RUN curl https://sh.rustup.rs -s > /home/install.sh && \
    chmod +x /home/install.sh && \
    sh /home/install.sh -y --verbose --default-toolchain nightly

ENV PATH "/root/.cargo/bin:$PATH"

ENV SOURCES=/sources

RUN mkdir -p $SOURCES

ADD ./ $SOURCES

WORKDIR $SOURCES

RUN rustup update

RUN cargo update

RUN cargo build --release

EXPOSE 80
EXPOSE 8080

# CMD ROCKET_ENV=development ./target/release/rocket-rust

CMD ROCKET_ENV=production ./target/release/rocket-rust
```


<br>

### referencias

- [documentación oficial de Rust](https://doc.rust-lang.org/stable/book/)
- [Rocket](https://rocket.rs)
- [HomeInside/rocket-rust](https://github.com/HomeInside/rocket-rust)
