+++
title = "World of Goo"
date = 2016-02-07

[taxonomies]
tags = ["archive", "JS"]

[extra]
authors = ["Diniremix"]
+++

[World of Goo](https://store.steampowered.com/app/22000/World_of_Goo) es un videojuego de lógica, con un fuerte énfasis en la física, Producido por [2D Boy](https://2dboy.com), una desarrolladora de videojuegos independiente compuesta por Kyle Gabler y Ron Carmel, ambos exempleados de Electronic Arts.
<!-- more -->


No hablaremos del juego en sí, sino de unos curiosos y fascinantes mensajes que aparecen cuando el juego va iniciando, hemos hecho una recopilacion de estos (no sé si están todos) e hicimos una función en **Javascript** que genera mensajes de Goo aleatorios. :smile:


```js
var messages =[
  'Excediendo la capacidad de la CPU',
  'Construyendo capacidad emocional',
  'Comprobando ozono',
  'Cargando',
  'Agitando',
  'Extrayendo',
  'Girando con violencia alrededor del eje de las Y',
  'Debatiendo el arte de los videojuegos',
  'Despreocupandose de los problemas de pareja',
  'Metiendo espectativas en sacos de arena',
  'Licuando Bytes',
  'Yodizando',
  'Probando la perfección',
  'Revolviendo la independencia',
  'Filtrando la moral',
  'Diagramando meticulosamente la diversión',
  'Homogeneizando Goo',
  'Aplacando los editores',
  'Destilando belleza',
  'Autofirmando',
  'Cambiando tiempo por espacio',
  'Fabricando responsabilidad social',
  'Reimaginando con exactitud',
  'Doblando la cuchara',
  'Construyendo una narración no lineal',
  'Aplicando innovación'
];

var tot = messages.length;

function getGoo (max=26) {
  for (i = 0; i < max; i++) {
    var pos = Math.floor(Math.random() * tot);
    var message = messages[pos];
    console.info(message, '...OK');
  }
}

getGoo(7);
```

### referencias

- [Sitio oficial](https://2dboy.com)
- [Wikipedia](https://es.wikipedia.org/wiki/World_of_Goo)
- [Fandom](https://worldofgoo.fandom.com/wiki/World_of_Goo)
- [Steam](https://store.steampowered.com/app/22000/World_of_Goo)
